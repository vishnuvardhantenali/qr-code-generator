﻿using Microsoft.Win32;
using QRCoder;
using System;
using System.Drawing;
using System.Security.Cryptography;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Color = System.Drawing.Color;

namespace QRCodeGeneratorWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // Customize the QR code colors
        Color darkColor = Color.Blue;
        Color lightColor = Color.Transparent;

        // Generate a random key and IV
        byte[] key = new byte[32];
        byte[] iv = new byte[16];
        public string encryptedData { get; set; }

        /// <summary>
        /// Generate the QR Code by chnaging the Text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            string textToEncode = txtInput.Text.Trim();

            if (!string.IsNullOrEmpty(textToEncode))
            {
                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode(textToEncode, QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);

                Bitmap qrCodeImage = qrCode.GetGraphic(20, darkColor, lightColor, true);

                // Convert the Bitmap image to a WPF ImageSource
                BitmapSource wpfBitmap = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    qrCodeImage.GetHbitmap(),
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());

                imgQRCode.Source = wpfBitmap;

                // Dispose of the generated Bitmap
                qrCodeImage.Dispose();
            }
        }

        /// <summary>
        /// Save the QR code button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQrScanner_Click(object sender, RoutedEventArgs e)
        {
            string textToEncode = txtInput.Text.Trim();

            if (!string.IsNullOrEmpty(textToEncode))
            {
                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode(textToEncode, QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);


                Bitmap qrCodeImage = qrCode.GetGraphic(20, darkColor, lightColor, true);

                // Show the SaveFileDialog to select a file path
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "PNG Image|*.png";
                saveFileDialog.FileName = "QRCode.png";

                if (saveFileDialog.ShowDialog() == true)
                {
                    string filePath = saveFileDialog.FileName;

                    // Save the QR code image to the selected file path
                    qrCodeImage.Save(filePath, System.Drawing.Imaging.ImageFormat.Png);

                    MessageBox.Show($"QR code image saved to {filePath}");
                }

                qrCodeImage.Dispose();
            }
        }

        /// <summary>
        /// Encrypt button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEncryptQr_Click(object sender, RoutedEventArgs e)
        {
            string plainText = txtEncryptQr.Text.Trim();

            if (!string.IsNullOrEmpty(plainText))
            {
                try
                {
                    // Encrypt the data
                    encryptedData = EncryptAES(plainText);

                    // Generate QR code using the encrypted data
                    GenerateQRCode(encryptedData, true);
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error encrypting and generating QR code: {ex.Message}");
                }
            }
            else
            {
                MessageBox.Show("Please enter plain text.");
            }
        }

        /// <summary>
        /// Encrypt the string
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        private string EncryptAES(string plainText)
        {
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = key;
                aesAlg.IV = iv;

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                byte[] encryptedBytes;

                using (System.IO.MemoryStream msEncrypt = new System.IO.MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (System.IO.StreamWriter swEncrypt = new System.IO.StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        }
                    }
                    encryptedBytes = msEncrypt.ToArray();
                }

                return Convert.ToBase64String(encryptedBytes);
            }
        }

        /// <summary>
        /// Generation of the QR Code Image
        /// </summary>
        /// <param name="data"></param>
        /// <param name="isEncrypt"></param>
        private void GenerateQRCode(string data, bool isEncrypt = true)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(data, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);

            Bitmap qrCodeImage = qrCode.GetGraphic(20);

            // Convert the Bitmap image to a WPF ImageSource
            BitmapSource wpfBitmap = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                qrCodeImage.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());
            if (isEncrypt)
                imgEncryptQrCode.Source = wpfBitmap;
            else
                imgDecryptQrCode.Source = wpfBitmap;
        }

        /// <summary>
        /// Decrypt button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDecryptQr_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(encryptedData))
            {
                try
                {
                    // Decrypt the data                    
                    string decryptedString = DecryptString(encryptedData, key, iv);

                    // Display decrypted data
                    txtDecryptQr.Text = decryptedString;
                    // Generate QR code using the encrypted data
                    GenerateQRCode(decryptedString, false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error decrypting QR code: {ex.Message}");
                }
            }
            else
            {
                MessageBox.Show("Please enter encrypted data.");
            }
        }

        /// <summary>
        /// Decrypt the string
        /// </summary>
        /// <param name="cipherText"></param>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <returns></returns>
        static string DecryptString(string cipherText, byte[] key, byte[] iv)
        {
            string plaintext = null;

            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = iv;

                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                byte[] cipherBytes = Convert.FromBase64String(cipherText);

                using (var ms = new System.IO.MemoryStream(cipherBytes))
                {
                    using (var cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                    {
                        using (var sr = new System.IO.StreamReader(cs))
                        {
                            plaintext = sr.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }
    }
}